# System administrator task:  
physical/virtual ubuntu machine bootstrap:  

## manual ubuntu install  
Install Ubuntu LTS on host  

## run ssh client to create folder .ssh
ssh hjo6120@bitbucket.org


## manual SSH key install  
Copy SSH private and public keys from password manager, to ~/.ssh, then:  
chmod 600 ~/.ssh/id_rsa_bitbucket  

## auto install ansible and git  
wget --no-cache -qO- https://bitbucket.org/hjo1620/bootstrap/raw/master/lxdhost | sh -s;  

## auto init dotfiles and bin
ansible-playbook -v -K ~/git/bootstrap/ansible/0010-home.yml

## auto lxd
ansible-playbook -v -K ~/git/bootstrap/ansible/0020-lxd.yml

